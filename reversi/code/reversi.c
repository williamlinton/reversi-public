#define TEXT_ENABLED 1
#define MOUSE_WHEEL_PIXELS_PER_SCROLL 30
#define MOUSE_WHEEL_SCROLL_DURATION_MS 200

char STR_CLEAR_ALL_NOTES[16] = "Clear all notes";
char STR_DEBUG_WAVE_FORMS[17] = "Debug wave forms";
char STR_CURRENT_CHORD_LABEL_BASE[9] = "Current ";
char STR_CURRENT_CHORD_LABEL_NOTE[10] = "note:";
char STR_CURRENT_CHORD_LABEL_INTERVAL[10] = "interval:";
char STR_CURRENT_CHORD_LABEL_CHORD[10] = "chord:";

char STR_CURRENT_PLAYER_LABEL[17] = "Current Player: ";

void 
RenderDot(game_offscreen_buffer *Buffer, int Color, int XOffset, int YOffset)
{
#ifdef USE_OPENGL0
    glBegin(GL_TRIANGLES);
    
#else
    if (XOffset >= Buffer->Width)
    {
        XOffset = Buffer->Width - 1;
    }
    if (XOffset < 0)
    {
        XOffset = 0;
    }

    if (YOffset >= Buffer->Height)
    {
        YOffset = Buffer->Height - 1;
    }
    if (YOffset < 0)
    {
        YOffset = 0;
    }

    uint8 *Row = (uint8 *)Buffer->Memory + (YOffset*Buffer->Width)*Buffer->BytesPerPixel;
    uint32 *Pixel = (uint32 *)Row + XOffset;
    *Pixel = Color;
#endif
}

void
RenderStuff(game_offscreen_buffer Buffer, int BlueOffset, int GreenOffset)
{
    uint8 *Row = (uint8 *)Buffer.Memory;
    for(int Y = 0; Y < Buffer.Height; ++Y)
    {
        uint32 *Pixel = (uint32 *)Row;
        for(int X = 0; X < Buffer.Width; ++X)
        {
            uint8 Blue = (X + BlueOffset);
            uint8 Green = (X + GreenOffset);

            *Pixel++ = ((Green << 8) | Blue);
        }

        Row += Buffer.Pitch;
    }
}

int
GetBufferOffset(game_offscreen_buffer *Buffer, int x, int y)
{
    return (y*Buffer->Width + x)*Buffer->BytesPerPixel;
}

#if 0
void
RenderCircleHorizontalHalf(
{
    
    for (int s = 0; s < Segments; s++)
    {
        real32 theta = (s + 1)/Segments * 1 * Pi32;
        real32 posx = sin(theta);
        real32 posy = cos(theta);
        int pixelx = (int)posx;
        int pixely = (int)posy;
        real32 distancex = XCenter - pixelx;
        real32 distancey = YCenter - pixely;
        int memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        uint32 *Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        *Pixel = StrokeColor;
        
        for (int x = pixelx; x < distancex * 2 - 1; x++)
        {
            *Pixel++ = FillColor;
        }

        *Pixel = StrokeColor;
    }
}
#endif

void
RenderCircle(game_offscreen_buffer *Buffer, int StrokeColor, int FillColor, int XCenter, int YCenter, int Radius, int Segments)
{
    //uint8 *Row = (uint8 *)Buffer->Memory + (YOffset*Buffer->Width)*Buffer->BytesPerPixel;
    
    for (int s = 0; s < Segments; s++)
    {
        real32 theta = (real32)(s + 1)/(real32)Segments * 0.5 * Pi32;
        real32 soh = sin(theta);
        int distancey = (int)(soh * Radius);
        real32 cah = cos(theta);
        int distancex = (int)(cah * Radius);
        int pixelx = XCenter - distancex;
        int pixely = YCenter - distancey;
        int memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        uint32 *Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        *Pixel = StrokeColor;
        
        for (int x = 0; x < distancex * 2 - 1; x++)
        {
            *Pixel++ = FillColor;
        }

        *Pixel = StrokeColor;

        #if 0
        memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        
        for (int y = pixely; y < distancey * 2 - 1; y++)
        {
            memoryOffset = GetBufferOffset(Buffer, pixelx, y);
            Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
            *Pixel = FillColor;
        }
        #endif
    }
    
    for (int s = 0; s < Segments; s++)
    {
        real32 theta = 2.0 * Pi32 - (real32)(s + 1)/(real32)Segments * 0.5 * Pi32;
        real32 soh = sin(theta);
        int distancey = (int)(soh * Radius);
        real32 cah = cos(theta);
        int distancex = (int)(cah * Radius);
        int pixelx = XCenter - distancex;
        int pixely = YCenter - distancey;
        int memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        uint32 *Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        *Pixel = StrokeColor;
        
        for (int x = 0; x < distancex * 2 - 1; x++)
        {
            *Pixel++ = FillColor;
        }

        *Pixel = StrokeColor;

        #if 0
        memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        
        for (int y = pixely; y < distancey * 2 - 1; y++)
        {
            memoryOffset = GetBufferOffset(Buffer, pixelx, y);
            Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
            *Pixel = FillColor;
        }
        #endif
    }
    
    /*
    for (int s = 0; s < Segments; s++)
    {
        real32 theta = 2 * Pi32 - ((s + 1)/Segments * 0.5 * Pi32);
        real32 posx = sin(theta);
        real32 posy = cos(theta);
        int pixelx = (int)posx;
        int pixely = (int)posy;
        real32 distancex = XCenter - pixelx;
        real32 distancey = YCenter - pixely;
        int memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        uint32 *Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        *Pixel = StrokeColor;
        
        for (int x = pixelx; x < distancex * 2 - 1; x++)
        {
            *Pixel++ = FillColor;
        }

        *Pixel = StrokeColor;

        #if 0
        memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        
        for (int y = pixely; y < distancey * 2 - 1; y++)
        {
            memoryOffset = GetBufferOffset(Buffer, pixelx, y);
            Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
            *Pixel = FillColor;
        }
        #endif
    }
    */
    #if 0
    for(int Y = YOffset; Y < (YOffset + Height); ++Y)
    {
        uint32 *Pixel = (uint32 *)Row + XOffset;
        for(int X = XOffset; X < (XOffset + Width); ++X)
        {
            int Color;
            if (X == XOffset)
            {
                if (StrokeColorLeft == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorLeft;
                }
            }
            else if (X == (XOffset + Width - 1))
            {
                if (StrokeColorRight == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorRight;
                }
            }
            else if (Y == YOffset)
            {
                if (StrokeColorTop == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorTop;
                }
            }
            else if (Y == (YOffset + Height - 1))
            {
                if (StrokeColorBottom == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorBottom;
                }
            }
            else
            {
                if (FillColor == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = FillColor;
                }
            }
//            uint8 Blue = FillColor & 0xFF;
//            uint8 Green = (FillColor >> 8) & 0xFF;
//            uint8 Red = (FillColor >> 16) & 0xFF;

//            *Pixel++ = (((Red << 16) | (Green << 8)) | Blue);
            *Pixel++ = Color;
        }

        Row += Buffer->Pitch;
    }
    #endif

    //     A         R         G         B
    // 0000 0000 0000 0000 0000 0000 0000 0000


}

void
RenderRectangleWithBorders(game_offscreen_buffer *Buffer, int StrokeColorTop, int StrokeColorBottom, int StrokeColorLeft, int StrokeColorRight, int FillColor, int XOffset, int YOffset, int Width, int Height)
{
    uint8 *Row = (uint8 *)Buffer->Memory + (YOffset*Buffer->Width)*Buffer->BytesPerPixel;
    for(int Y = YOffset; Y < (YOffset + Height); ++Y)
    {
        uint32 *Pixel = (uint32 *)Row + XOffset;
        for(int X = XOffset; X < (XOffset + Width); ++X)
        {
            int Color;
            if (X == XOffset)
            {
                if (StrokeColorLeft == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorLeft;
                }
            }
            else if (X == (XOffset + Width - 1))
            {
                if (StrokeColorRight == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorRight;
                }
            }
            else if (Y == YOffset)
            {
                if (StrokeColorTop == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorTop;
                }
            }
            else if (Y == (YOffset + Height - 1))
            {
                if (StrokeColorBottom == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorBottom;
                }
            }
            else
            {
                if (FillColor == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = FillColor;
                }
            }
//            uint8 Blue = FillColor & 0xFF;
//            uint8 Green = (FillColor >> 8) & 0xFF;
//            uint8 Red = (FillColor >> 16) & 0xFF;

//            *Pixel++ = (((Red << 16) | (Green << 8)) | Blue);
            *Pixel++ = Color;
        }

        Row += Buffer->Pitch;
    }

    //     A         R         G         B
    // 0000 0000 0000 0000 0000 0000 0000 0000


}

void
RenderRectangle(game_offscreen_buffer *Buffer, int StrokeColor, int FillColor, int XOffset, int YOffset, int Width, int Height)
{
    RenderRectangleWithBorders(Buffer, 
                               StrokeColor, StrokeColor, StrokeColor, StrokeColor, 
                               FillColor, XOffset, YOffset, Width, Height);
}

internal bool32
CoordsWithinBounds(int StartX, int StartY, int Width, int Height, int CoordX, int CoordY)
{
    return (CoordX >= StartX) && (CoordX < (StartX + Width)) &&
           (CoordY >= StartY) && (CoordY < (StartY + Height));
}

void
RenderButtonControls(game_memory *Game, game_offscreen_buffer *Buffer)
{
    game_state *State = &Game->State;

    int ClearAllButton_HoverColor = 0x222222;
    int ClearAllButton_ClickColor = 0x333333;
    int ClearAllButton_BaseColor = 0x111111;

    int ClearAllButton_Color = ClearAllButton_BaseColor;
    int ClearAllButton_FontSize = 16;
    int ClearAllButton_Width = 200;
    int ClearAllButton_PaddingY = 20;
    int ClearAllButton_Height = ClearAllButton_FontSize + 2*ClearAllButton_PaddingY;

    int ClearAllButton_StartX = 200;
    int ClearAllButton_StartY = 50;

    if (CoordsWithinBounds(ClearAllButton_StartX, ClearAllButton_StartY,
                           ClearAllButton_Width, ClearAllButton_Height,
                           State->UserInput.MouseX, 
                           State->UserInput.MouseY))
    {
        if (State->UserInput.MouseLeftButtonIsDown)
        {
            ClearAllButton_Color = ClearAllButton_ClickColor;
        }
        else
        {
            ClearAllButton_Color = ClearAllButton_HoverColor;
            if (State->UserInput.MouseLeftButtonClicked)
            {
            }
        }
    }

    RenderRectangle(Buffer, 
                    ClearAllButton_Color, ClearAllButton_Color, 
                    ClearAllButton_StartX, ClearAllButton_StartY, 
                    ClearAllButton_Width, ClearAllButton_Height);

#if TEXT_ENABLED
    WriteText(
        Game, 
        Buffer, 
        STR_CLEAR_ALL_NOTES,
        15, 
        ClearAllButton_FontSize, 
        ClearAllButton_StartX + 10, 
        ClearAllButton_StartY + ClearAllButton_FontSize + ClearAllButton_PaddingY,
        0xFFFFFFFF);
#endif

    int DebugWaveFormsButton_HoverColor = 0x222222;
    int DebugWaveFormsButton_EnabledColor = 0x333333;
    int DebugWaveFormsButton_BaseColor = 0x111111;

    int DebugWaveFormsButton_Color = DebugWaveFormsButton_BaseColor;
    int DebugWaveFormsButton_FontSize = 16;
    int DebugWaveFormsButton_Width = 200;
    int DebugWaveFormsButton_PaddingY = 20;
    int DebugWaveFormsButton_Height = DebugWaveFormsButton_FontSize + 2*DebugWaveFormsButton_PaddingY;

    int DebugWaveFormsButton_StartX = 200;
    int DebugWaveFormsButton_StartY = 150;
    int DebugWaveFormsButton_IsHovered = 0;

    if (CoordsWithinBounds(DebugWaveFormsButton_StartX, DebugWaveFormsButton_StartY,
                           DebugWaveFormsButton_Width, DebugWaveFormsButton_Height,
                           State->UserInput.MouseX, 
                           State->UserInput.MouseY))
    {
        if (State->UserInput.MouseLeftButtonClicked)
        {
            State->Settings.DebugWaveFormsEnabled = !State->Settings.DebugWaveFormsEnabled;
        }
        else
        {
            DebugWaveFormsButton_IsHovered = 1;
            DebugWaveFormsButton_Color = DebugWaveFormsButton_HoverColor;
        }
    }

    if (State->Settings.DebugWaveFormsEnabled)
    {
        DebugWaveFormsButton_Color = DebugWaveFormsButton_EnabledColor;
    }

    RenderRectangle(Buffer, 
                    DebugWaveFormsButton_Color, DebugWaveFormsButton_Color, 
                    DebugWaveFormsButton_StartX, DebugWaveFormsButton_StartY, 
                    DebugWaveFormsButton_Width, DebugWaveFormsButton_Height);

#if TEXT_ENABLED
    WriteText(
        Game, 
        Buffer, 
        STR_DEBUG_WAVE_FORMS,
        16, 
        DebugWaveFormsButton_FontSize, 
        DebugWaveFormsButton_StartX + 10, 
        DebugWaveFormsButton_StartY + DebugWaveFormsButton_FontSize + DebugWaveFormsButton_PaddingY,
        0xFFFFFFFF);
#endif

}

real32
Abs_real32(real32 num)
{
    if (num < 0)
    {
        return -num;
    }

    return num;
}

void
RenderCursor(game_offscreen_buffer *Buffer, user_input *UserInput)
{

    int CursorX = UserInput->MouseX - 2;
    int CursorY = UserInput->MouseY - 2;
    if (CursorX < 0)
    {
        CursorX = 0;
    }

    if (CursorX >= Buffer->Width)
    {
        CursorX = Buffer->Width - 1;
    }

    if (CursorY < 0)
    {
        CursorY = 0;
    }

    if (CursorY >= Buffer->Height)
    {
        CursorY = Buffer->Height - 1;
    }

    RenderRectangle(Buffer, 0xFF0000, 0xFF0000, CursorX, CursorY, 5, 5);
}

typedef enum Direction
{
    NORTH,
    NORTHEAST,
    EAST,
    SOUTHEAST,
    SOUTH,
    SOUTHWEST,
    WEST,
    NORTHWEST,
    DIRECTION_COUNT
} Direction;

int
GetNextCell(int discs[64], int currentCell, Direction direction)
{
    int cellCountPerSide = 8;
    int desiredCell;
    int y = currentCell / cellCountPerSide;
    int x = currentCell % cellCountPerSide;
    switch (direction)
    {
        case NORTH:     y -= 1;         break;
        case NORTHEAST: y -= 1; x += 1; break;
        case EAST:              x += 1; break;
        case SOUTHEAST: y += 1; x += 1; break;
        case SOUTH:     y += 1;         break;
        case SOUTHWEST: y += 1; x -= 1; break;
        case WEST:              x -= 1; break;
        case NORTHWEST: y -= 1; x -= 1; break;
    }

    if (x < 0 || y < 0 || x >= cellCountPerSide || y >= cellCountPerSide)
    {
        return -1;
    }

    return y * cellCountPerSide + x;
}

bool32
DoesDirectionFlipDiscs(int discs[64], int desiredMove, int currentPlayer, Direction direction)
{
    int cell = GetNextCell(discs, desiredMove, direction);
    if (cell >= 0)
    {
        int player = discs[cell];
        if (player && player != currentPlayer)
        {
            while ((cell = GetNextCell(discs, cell, direction)) >= 0)
            {
                int nextPlayer = discs[cell];
                if (nextPlayer == currentPlayer)
                {
                    return 1;
                }
            }
        }
    }

    return 0;
}

bool32
IsValidMove(int discs[64], int desiredMove, int currentPlayer)
{
    int foundValidMove = 0;
    for (int i = 0; i < DIRECTION_COUNT; i++)
    {
        if (DoesDirectionFlipDiscs(discs, desiredMove, currentPlayer, (Direction)i))
        {
            foundValidMove = 1;
            break;
        }
    }

    return foundValidMove;
}

void
FlipDiscs(int discs[64], int move, int currentPlayer)
{
    discs[move] = currentPlayer;
    for (int i = 0; i < DIRECTION_COUNT; i++)
    {
        if (DoesDirectionFlipDiscs(discs, move, currentPlayer, (Direction)i))
        {
            int cell = move;
            while ((cell = GetNextCell(discs, cell, (Direction)i)) >= 0 && discs[cell] != currentPlayer)
            {
                discs[cell] = currentPlayer;
            }
        }
    }
}

typedef struct player_label
{
    int Length;
    char *Label;
} player_label;

player_label PlayerLabels[2] = 
{
    { 6, "Black" },
    { 6, "White" }
};

void
RenderGameInfo(game_memory *Game, game_offscreen_buffer *Buffer, int x, int y)
{
    int CurrentPlayer_FontSize = 16;
    int CurrentPlayer_StartX = x;
    int CurrentPlayer_StartY = y;
    int CurrentPlayer_PaddingY = 0;
    int XOffset = WriteText(
        Game, 
        Buffer, 
        STR_CURRENT_PLAYER_LABEL,
        17, 
        CurrentPlayer_FontSize, 
        CurrentPlayer_StartX + 10,
        CurrentPlayer_StartY + CurrentPlayer_FontSize + CurrentPlayer_PaddingY,
        0xFFFFFFFF);

    player_label PlayerLabel = PlayerLabels[Game->State.CurrentPlayer - 1];
    
    WriteText(
        Game, 
        Buffer, 
        PlayerLabel.Label,
        PlayerLabel.Length, 
        CurrentPlayer_FontSize, 
        XOffset,
        CurrentPlayer_StartY + CurrentPlayer_FontSize + CurrentPlayer_PaddingY,
        0xFFFFFFFF);
    
}

void
RenderGameMessage(game_memory *Game, game_offscreen_buffer *Buffer, int x, int y, char *Message, int Length)
{
    int CurrentPlayer_FontSize = 16;
    int CurrentPlayer_StartX = x;
    int CurrentPlayer_StartY = y;
    int CurrentPlayer_PaddingY = 0;
    int XOffset = WriteText(
        Game, 
        Buffer, 
        Message,
        Length, 
        CurrentPlayer_FontSize, 
        CurrentPlayer_StartX + 10,
        CurrentPlayer_StartY + CurrentPlayer_FontSize + CurrentPlayer_PaddingY,
        0xFFFFFFFF);
}

void
GameUpdateAndRender(game_memory *Game, game_offscreen_buffer *Buffer)
{
    if (!Game->State.DiscsInitialized)
    {
        int discs[64] =
        {
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 2, 1, 0, 0, 0,
            0, 0, 0, 1, 2, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
        };
        memcpy(Game->State.Discs, discs, sizeof(int) * 64);
        Game->State.DiscsInitialized = 1;
        Game->State.CurrentPlayer = 1;
    }

    RenderRectangle(Buffer, 0x000000, 0x1B1B1B, 0, 0, Buffer->Width, Buffer->Height);

    int cellCountPerSide = 8;
    int cellSize = 80;
    int cellBorder = 1;
    int boardSize = cellSize * cellCountPerSide + cellBorder * (cellCountPerSide + 1);
    int cellColor = 0x004400;
    int cellBorderColor = 0xEEEEEE;
    int boardOriginX = 10;
    int boardOriginY = 10;
    
    int hoveredCell = -1;
    int moveWasMade = 0;

    // Render board
    for (int y = 0; y < cellCountPerSide; y++)
    {
        for (int x = 0; x < cellCountPerSide; x++)
        {
            int cellX = boardOriginX + x * (cellSize - 1);
            if (cellX < 0) cellX = 0;
            int cellY = boardOriginY + y * (cellSize - 1);
            if (cellY < 0) cellY = 0;
            int thisCellColor = cellColor;
            #if 1
            int borderColor = 0xFFFFFF;
            #else
            int borderColor = 0x000000;
            if (y % 2) borderColor = 0x00DD00;
            else if (y % 3) borderColor = 0xDD0000;
            else if (x % 2) borderColor = 0x0000DD;
            else if (x % 3) borderColor = 0xFFFFFF;
            #endif
            int discValue = Game->State.Discs[y * cellCountPerSide + x];
            if (discValue == 0 && Game->State.UserInput.MouseX > cellX && Game->State.UserInput.MouseX < cellX + cellSize &&
                Game->State.UserInput.MouseY > cellY && Game->State.UserInput.MouseY < cellY + cellSize)
            {
                hoveredCell = y * cellCountPerSide + x;
                thisCellColor = 0x006600;
                if (Game->State.UserInput.MouseLeftButtonClicked)
                {
                    thisCellColor = cellColor;
                    if (IsValidMove(Game->State.Discs, hoveredCell, Game->State.CurrentPlayer))
                    {
                        FlipDiscs(Game->State.Discs, hoveredCell, Game->State.CurrentPlayer);
                        Game->State.CurrentPlayer ^= 3;
                        moveWasMade = 1;
                    }

                }
                else if (Game->State.UserInput.MouseLeftButtonIsDown)
                {
                    thisCellColor = 0x008800;
                }
            }
            RenderRectangle(Buffer, borderColor, thisCellColor, cellX, cellY, cellSize, cellSize);
            if (discValue > 0)
            {
                int centerX = (int)(cellX + (real32)cellSize / 2.0);
                int centerY = (int)(cellY + (real32)cellSize / 2.0);

                if (discValue == 1)
                {
                    RenderCircle(Buffer, 0x000000, 0x000000, centerX, centerY, 30, 80);
                }
                else if (discValue == 2)
                {
                    RenderCircle(Buffer, 0xFFFFFF, 0xFFFFFF, centerX, centerY, 30, 80);
                }
            }
        }
    }

    if (moveWasMade)
    {
        int foundValidMoves = 0;
        for (int y = 0; y < cellCountPerSide; y++)
        {
            for (int x = 0; x < cellCountPerSide; x++)
            {
                int cell = y * cellCountPerSide + x;
                if (Game->State.Discs[cell] == 0)
                {
                    if (IsValidMove(Game->State.Discs, cell, Game->State.CurrentPlayer))
                    {
                        foundValidMoves = 1;
                        break;
                    }
                }
            }
            if (foundValidMoves)
            {
                break;
            }
        }
        if (!foundValidMoves)
        {
            if (Game->State.CurrentPlayer == 1)
            {
                char Message[32] = "No moves left for player Black!";
                RenderGameMessage(Game, Buffer, boardSize, 50, Message, 32);
                Game->State.CurrentPlayer = 2;
            }
            else
            {
                char Message[32] = "No moves left for player White!";
                RenderGameMessage(Game, Buffer, boardSize, 50, Message, 32);
                Game->State.CurrentPlayer = 1;
            }
        }
    }

    RenderGameInfo(Game, Buffer, boardSize, 20);
}





