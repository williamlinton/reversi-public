/* **************************************************************************
   $File: $
   $Date: $
   $Revision: $
   $Creator: William Linton $
   $Notice: (C) Copyright 2015 by William Linton, Inc. All Rights Reserved. $
   ************************************************************************** */

#include <stdio.h>
#include <math.h>
#include "SDL.h"

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#endif

#if 0
#define PRINT(...) { char str[256]; sprintf(str, __VA_ARGS__); OutputDebugString(str); }
#endif

#define PRINT(...) printf(__VA_ARGS__);


#define internal static
#define local_persist static
#define global_variable static

#define USE_OPENGL 1

#define Pi32 3.14159265359f

#define ArrayCount(Array) (sizeof(Array) / sizeof(Array[0]))

#include "win32_reversi.h"
#include "reversi_c.h"
#include "reversi_text.c"
#include "reversi.c"

#define FRAME_DIAG 1

global_variable int GlobalRunning;

SDL_Window *ProgramWindow;
game_offscreen_buffer Buffer = {0};

game_memory Game = {0};

SDL_Renderer *renderer = NULL;
SDL_Texture *screenTexture = NULL;
SDL_Texture *renderTexture = NULL;

void render(SDL_Texture *texture, game_memory *Game, game_offscreen_buffer *Buffer)
{
    void *pixels = 0;
    int pitch = 0;
    int err = SDL_LockTexture(texture, NULL, &pixels, &pitch);
    if (!err)
    {
        //PRINT("Mouse: %4d,%4d \n", Game->State.UserInput.MouseX, Game->State.UserInput.MouseY);
        GameUpdateAndRender(Game, Buffer);
        SDL_UnlockTexture(texture);
        memcpy(pixels, Buffer->Memory, Buffer->Width * Buffer->Height * Buffer->BytesPerPixel);
        SDL_RenderCopy(renderer, texture, NULL, NULL);
        SDL_RenderPresent(renderer);
        //SDL_UpdateTexture(texture, NULL, Buffer, InitialWidth * 4);
    }
    else
    {
        const char *errorMessage = SDL_GetError();
        printf("SDL_LockTexture failed: %s\n", errorMessage);
    }
}

int HandleEvent(game_memory *Game, SDL_Event *Event)
{
    game_state *GameState = &Game->State;
    int ShouldQuit = 0;
    switch (Event->type)
    {
        case SDL_QUIT:
        {
            PRINT("SDL_QUIT\n");
            ShouldQuit = 1;
        } break;

        case SDL_WINDOWEVENT:
        {
            switch (Event->window.event)
            {
                case SDL_WINDOWEVENT_RESIZED:
                {
                    PRINT("SDL_WINDOWEVENT_RESIZED (%d, %d)\n",
                            Event->window.data1,
                            Event->window.data2);
                    render(screenTexture, Game, &Buffer);
                } break;
            }
        } break;

        case SDL_KEYUP:
        {
            SDL_Keycode KeyCode = Event->key.keysym.sym;
            if (KeyCode == SDLK_ESCAPE)
            {
                ShouldQuit = 1;
            }

        } break;

        case SDL_MOUSEBUTTONDOWN:
        {
            SDL_MouseButtonEvent *event = (SDL_MouseButtonEvent *)Event;
            GameState->UserInput.MouseLeftButtonIsDown = 1;
            GameState->UserInput.MouseX = event->x;
            GameState->UserInput.MouseY = event->y;
            GameState->UserInput.StartedDraggingAtMouseX = event->x;
            GameState->UserInput.StartedDraggingAtMouseY = event->y;
            //Win32CorrectMousePosition(Window, GameState);
        } break;
        case SDL_MOUSEBUTTONUP:
        {
            SDL_MouseButtonEvent *event = (SDL_MouseButtonEvent *)Event;
            GameState->UserInput.MouseLeftButtonIsDown = 0;
            GameState->UserInput.MouseLeftButtonClicked = 1;
            GameState->UserInput.MouseX = event->x;
            GameState->UserInput.MouseY = event->y;
            //Win32CorrectMousePosition(Window, GameState);
        } break;

        case SDL_MOUSEMOTION:
        {
            SDL_MouseMotionEvent *event = (SDL_MouseMotionEvent *)Event;
            GameState->UserInput.MouseX = event->x;
            GameState->UserInput.MouseY = event->y;
            //Win32CorrectMousePosition(Window, GameState);
        } break;

        case SDL_MOUSEWHEEL:
        {
        } break;
                    
    }

    return(ShouldQuit);
}

void loop()
{
    SDL_Event Event;
    while (SDL_PollEvent(&Event))
    {
        if (HandleEvent(&Game, &Event))
        {
            GlobalRunning = 0;
            break;
        }
    }

    /*
    float ElapsedTime = SDL_GetTicks();
    float TimeDiff = ElapsedTime - PreviousElapsedTime;
    if (TimeDiff >= flipAtMs)
    {
        PRINT("Redraw at %f ms!\n", TimeDiff);
        DrawScreen(InitialWidth, InitialHeight);
        float PostRenderElapsedTime = SDL_GetTicks();
        float TotalFrameTime = PostRenderElapsedTime -
            PreviousElapsedTime;
        float RenderTime = PostRenderElapsedTime - ElapsedTime;
        flipAtMs = targetMsPerFrame - RenderTime - 1;

        PreviousElapsedTime = PostRenderElapsedTime;
        PRINT("Total frame time: %f ms!\n", TotalFrameTime);
    }
    */

    render(renderTexture, &Game, &Buffer);
    Game.State.UserInput.MouseLeftButtonClicked = 0;
}

int main (int argc, char *argv[])
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        // TODO: SDL_Init didn't work!
    }

    int InitialWidth = 1280;
    int InitialHeight = 720;

    renderer = NULL;

    ProgramWindow = SDL_CreateWindow("Reversi",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            InitialWidth,
            InitialHeight,
            SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

    renderer = SDL_CreateRenderer(ProgramWindow, -1, SDL_RENDERER_ACCELERATED);
    screenTexture = SDL_GetRenderTarget(renderer);
    renderTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, InitialWidth, InitialHeight);

    int targetFPS = 60;
    float targetMsPerFrame = (1.0f / (float)targetFPS) * 1000.0f;


    //-------- Font Init ----------
    Game.FontData = (unsigned char *)malloc(1000000);
    InitText(&Game);

    game_state State = {0};
    Game.State = State;

    user_input Input = {0};
    Game.State.UserInput = Input;

    Buffer.BytesPerPixel = 4;
    Buffer.Width = InitialWidth;
    Buffer.Height = InitialHeight;
    Buffer.Memory = malloc(Buffer.Width * Buffer.Height * Buffer.BytesPerPixel);
    Buffer.Pitch = Buffer.Width * Buffer.BytesPerPixel;

    GlobalRunning = 1;

#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(loop, targetFPS, 1);
#else

    while(GlobalRunning)
    {
        float startFrame = SDL_GetTicks();
        loop();
        float endFrame = SDL_GetTicks();
        float durationFrame = endFrame - startFrame;
#if FRAME_DIAG
        char msg[20];
        snprintf(msg, 19, "Time: %f\r\n", durationFrame);
        PRINT(msg);
#endif
        int delay = (int)(targetMsPerFrame - durationFrame);
        if (delay && GlobalRunning)
        {
            SDL_Delay(delay);
        }
    }

#endif

    //SDL_GL_DeleteContext(mainContext);
    SDL_DestroyWindow(ProgramWindow);
    SDL_Quit();

    return 0;
}
