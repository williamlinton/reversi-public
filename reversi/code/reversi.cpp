#define TEXT_ENABLED 1
#define MOUSE_WHEEL_PIXELS_PER_SCROLL 30
#define MOUSE_WHEEL_SCROLL_DURATION_MS 200

char STR_CLEAR_ALL_NOTES[16] = "Clear all notes";
char STR_DEBUG_WAVE_FORMS[17] = "Debug wave forms";
char STR_CURRENT_CHORD_LABEL_BASE[9] = "Current ";
char STR_CURRENT_CHORD_LABEL_NOTE[10] = "note:";
char STR_CURRENT_CHORD_LABEL_INTERVAL[10] = "interval:";
char STR_CURRENT_CHORD_LABEL_CHORD[10] = "chord:";

char STR_CURRENT_PLAYER_LABEL[17] = "Current Player: ";

int
GetBufferOffset(game_offscreen_buffer *Buffer, int x, int y)
{
    return (y*Buffer->Width + x)*Buffer->BytesPerPixel;
}

void
RenderCircle(game_offscreen_buffer *Buffer, int StrokeColor, int FillColor, int XCenter, int YCenter, int Radius, int Segments)
{
    //uint8 *Row = (uint8 *)Buffer->Memory + (YOffset*Buffer->Width)*Buffer->BytesPerPixel;
    
    for (int s = 0; s < Segments; s++)
    {
        real32 theta = (real32)(s + 1)/(real32)Segments * 0.5 * Pi32;
        real32 soh = sin(theta);
        int distancey = (int)(soh * Radius);
        real32 cah = cos(theta);
        int distancex = (int)(cah * Radius);
        int pixelx = XCenter - distancex;
        int pixely = YCenter - distancey;
        int memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        uint32 *Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        *Pixel = StrokeColor;
        
        for (int x = 0; x < distancex * 2 - 1; x++)
        {
            *Pixel++ = FillColor;
        }

        *Pixel = StrokeColor;

        #if 0
        memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        
        for (int y = pixely; y < distancey * 2 - 1; y++)
        {
            memoryOffset = GetBufferOffset(Buffer, pixelx, y);
            Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
            *Pixel = FillColor;
        }
        #endif
    }
    
    for (int s = 0; s < Segments; s++)
    {
        real32 theta = 2.0 * Pi32 - (real32)(s + 1)/(real32)Segments * 0.5 * Pi32;
        real32 soh = sin(theta);
        int distancey = (int)(soh * Radius);
        real32 cah = cos(theta);
        int distancex = (int)(cah * Radius);
        int pixelx = XCenter - distancex;
        int pixely = YCenter - distancey;
        int memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        uint32 *Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        *Pixel = StrokeColor;
        
        for (int x = 0; x < distancex * 2 - 1; x++)
        {
            *Pixel++ = FillColor;
        }

        *Pixel = StrokeColor;

        #if 0
        memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        
        for (int y = pixely; y < distancey * 2 - 1; y++)
        {
            memoryOffset = GetBufferOffset(Buffer, pixelx, y);
            Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
            *Pixel = FillColor;
        }
        #endif
    }
    
    /*
    for (int s = 0; s < Segments; s++)
    {
        real32 theta = 2 * Pi32 - ((s + 1)/Segments * 0.5 * Pi32);
        real32 posx = sin(theta);
        real32 posy = cos(theta);
        int pixelx = (int)posx;
        int pixely = (int)posy;
        real32 distancex = XCenter - pixelx;
        real32 distancey = YCenter - pixely;
        int memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        uint32 *Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        *Pixel = StrokeColor;
        
        for (int x = pixelx; x < distancex * 2 - 1; x++)
        {
            *Pixel++ = FillColor;
        }

        *Pixel = StrokeColor;

        #if 0
        memoryOffset = GetBufferOffset(Buffer, pixelx, pixely);
        Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
        
        for (int y = pixely; y < distancey * 2 - 1; y++)
        {
            memoryOffset = GetBufferOffset(Buffer, pixelx, y);
            Pixel = (uint32 *)((uint8 *)Buffer->Memory + memoryOffset);
            *Pixel = FillColor;
        }
        #endif
    }
    */
    #if 0
    for(int Y = YOffset; Y < (YOffset + Height); ++Y)
    {
        uint32 *Pixel = (uint32 *)Row + XOffset;
        for(int X = XOffset; X < (XOffset + Width); ++X)
        {
            int Color;
            if (X == XOffset)
            {
                if (StrokeColorLeft == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorLeft;
                }
            }
            else if (X == (XOffset + Width - 1))
            {
                if (StrokeColorRight == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorRight;
                }
            }
            else if (Y == YOffset)
            {
                if (StrokeColorTop == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorTop;
                }
            }
            else if (Y == (YOffset + Height - 1))
            {
                if (StrokeColorBottom == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorBottom;
                }
            }
            else
            {
                if (FillColor == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = FillColor;
                }
            }
//            uint8 Blue = FillColor & 0xFF;
//            uint8 Green = (FillColor >> 8) & 0xFF;
//            uint8 Red = (FillColor >> 16) & 0xFF;

//            *Pixel++ = (((Red << 16) | (Green << 8)) | Blue);
            *Pixel++ = Color;
        }

        Row += Buffer->Pitch;
    }
    #endif

    //     A         R         G         B
    // 0000 0000 0000 0000 0000 0000 0000 0000


}

void
RenderRectangleWithBorders(game_offscreen_buffer *Buffer, int StrokeColorTop, int StrokeColorBottom, int StrokeColorLeft, int StrokeColorRight, int FillColor, int XOffset, int YOffset, int Width, int Height)
{
    uint8 *Row = (uint8 *)Buffer->Memory + (YOffset*Buffer->Width)*Buffer->BytesPerPixel;
    for(int Y = YOffset; Y < (YOffset + Height); ++Y)
    {
        uint32 *Pixel = (uint32 *)Row + XOffset;
        for(int X = XOffset; X < (XOffset + Width); ++X)
        {
            int Color;
            if (X == XOffset)
            {
                if (StrokeColorLeft == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorLeft;
                }
            }
            else if (X == (XOffset + Width - 1))
            {
                if (StrokeColorRight == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorRight;
                }
            }
            else if (Y == YOffset)
            {
                if (StrokeColorTop == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorTop;
                }
            }
            else if (Y == (YOffset + Height - 1))
            {
                if (StrokeColorBottom == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = StrokeColorBottom;
                }
            }
            else
            {
                if (FillColor == -1)
                {
                    Color = *Pixel;
                }
                else
                {
                    Color = FillColor;
                }
            }
//            uint8 Blue = FillColor & 0xFF;
//            uint8 Green = (FillColor >> 8) & 0xFF;
//            uint8 Red = (FillColor >> 16) & 0xFF;

//            *Pixel++ = (((Red << 16) | (Green << 8)) | Blue);
            *Pixel++ = Color;
        }

        Row += Buffer->Pitch;
    }

    //     A         R         G         B
    // 0000 0000 0000 0000 0000 0000 0000 0000


}

void
RenderRectangle(game_offscreen_buffer *Buffer, int StrokeColor, int FillColor, int XOffset, int YOffset, int Width, int Height)
{
    RenderRectangleWithBorders(Buffer, 
                               StrokeColor, StrokeColor, StrokeColor, StrokeColor, 
                               FillColor, XOffset, YOffset, Width, Height);
}

internal bool32
CoordsWithinBounds(int StartX, int StartY, int Width, int Height, int CoordX, int CoordY)
{
    return (CoordX >= StartX) && (CoordX < (StartX + Width)) &&
           (CoordY >= StartY) && (CoordY < (StartY + Height));
}

real32
Abs_real32(real32 num)
{
    if (num < 0)
    {
        return -num;
    }

    return num;
}

enum Direction
{
    NORTH,
    NORTHEAST,
    EAST,
    SOUTHEAST,
    SOUTH,
    SOUTHWEST,
    WEST,
    NORTHWEST,
    DIRECTION_COUNT
};

int
GetNextCell(int discs[64], int currentCell, Direction direction)
{
    int cellCountPerSide = 8;
    int desiredCell;
    int y = currentCell / cellCountPerSide;
    int x = currentCell % cellCountPerSide;
    switch (direction)
    {
        case NORTH:     y -= 1;         break;
        case NORTHEAST: y -= 1; x += 1; break;
        case EAST:              x += 1; break;
        case SOUTHEAST: y += 1; x += 1; break;
        case SOUTH:     y += 1;         break;
        case SOUTHWEST: y += 1; x -= 1; break;
        case WEST:              x -= 1; break;
        case NORTHWEST: y -= 1; x -= 1; break;
    }

    if (x < 0 || y < 0 || x >= cellCountPerSide || y >= cellCountPerSide)
    {
        return -1;
    }

    return y * cellCountPerSide + x;
}

bool32
DoesDirectionFlipDiscs(int discs[64], int desiredMove, int currentPlayer, Direction direction)
{
    int cell = GetNextCell(discs, desiredMove, direction);
    if (cell >= 0)
    {
        int player = discs[cell];
        if (player && player != currentPlayer)
        {
            while ((cell = GetNextCell(discs, cell, direction)) >= 0)
            {
                int nextPlayer = discs[cell];
                if (nextPlayer == currentPlayer)
                {
                    return 1;
                }
            }
        }
    }

    return 0;
}

bool32
IsValidMove(int discs[64], int desiredMove, int currentPlayer)
{
    int foundValidMove = 0;
    for (int i = 0; i < DIRECTION_COUNT; i++)
    {
        if (DoesDirectionFlipDiscs(discs, desiredMove, currentPlayer, (Direction)i))
        {
            foundValidMove = 1;
            break;
        }
    }

    return foundValidMove;
}

typedef enum flip_type
{
    MOVE,
    HOVER,
    FLIP_TYPE_LENGTH
} flip_type;

void
FlipDiscs(int discs[64], int hoverDiscs[64], int move, int currentPlayer, flip_type flipType)
{
    discs[move] = currentPlayer;
    for (int i = 0; i < DIRECTION_COUNT; i++)
    {
        if (DoesDirectionFlipDiscs(discs, move, currentPlayer, (Direction)i))
        {
            int cell = move;
            while ((cell = GetNextCell(discs, cell, (Direction)i)) >= 0 && discs[cell] != currentPlayer)
            {
                if (flipType == MOVE)
                {
                    discs[cell] = currentPlayer;
                }
                else
                {
                    hoverDiscs[cell] = currentPlayer;
                }
            }
        }
    }
}

typedef struct player_label
{
    int Length;
    char *Label;
} player_label;

player_label PlayerLabels[2] = 
{
    { 6, "Black" },
    { 6, "White" }
};

void
RenderGameInfo(game_memory *Game, game_offscreen_buffer *Buffer, int x, int y)
{
    if (Game->State.GameDone)
    {
        int CurrentPlayer_FontSize = 18;
        int CurrentPlayer_StartX = x;
        int CurrentPlayer_StartY = y;
        int CurrentPlayer_PaddingY = 0;
        char Message[21];
        int MessageLength = 0;
        if (Game->State.Winner == 1)
        {
            snprintf(Message, 18, "Player Black won!");
            MessageLength = 18;
        }
        else if (Game->State.Winner == 2)
        {
            snprintf(Message, 18, "Player White won!");
            MessageLength = 18;
        }
        else
        {
            snprintf(Message, 21, "The game was a draw!");
            MessageLength = 21;
        }
        int XOffset = WriteText(
            Game, 
            Buffer, 
            Message,
            MessageLength,
            CurrentPlayer_FontSize, 
            CurrentPlayer_StartX + 10,
            CurrentPlayer_StartY + CurrentPlayer_FontSize + CurrentPlayer_PaddingY,
            0xFFFFFFFF);
        
        char ScoreMessage[21];
        snprintf(ScoreMessage, 21, "Black: %2d, White: %2d", Game->State.Player1Score, Game->State.Player2Score);

        WriteText(
            Game, 
            Buffer, 
            ScoreMessage,
            21,
            CurrentPlayer_FontSize, 
            CurrentPlayer_StartX + 10,
            CurrentPlayer_StartY + (CurrentPlayer_FontSize + CurrentPlayer_PaddingY * 2) * 2,
            0xFFFFFFFF);
    }
    else
    {
        int CurrentPlayer_FontSize = 18;
        int CurrentPlayer_StartX = x;
        int CurrentPlayer_StartY = y;
        int CurrentPlayer_PaddingY = 0;
        int XOffset = WriteText(
            Game, 
            Buffer, 
            STR_CURRENT_PLAYER_LABEL,
            17, 
            CurrentPlayer_FontSize, 
            CurrentPlayer_StartX + 10,
            CurrentPlayer_StartY + CurrentPlayer_FontSize + CurrentPlayer_PaddingY,
            0xFFFFFFFF);

        player_label PlayerLabel = PlayerLabels[Game->State.CurrentPlayer - 1];
        
        WriteText(
            Game, 
            Buffer, 
            PlayerLabel.Label,
            PlayerLabel.Length, 
            CurrentPlayer_FontSize, 
            XOffset,
            CurrentPlayer_StartY + CurrentPlayer_FontSize + CurrentPlayer_PaddingY,
            0xFFFFFFFF);
    }
    
}

void
RenderGameMessage(game_memory *Game, game_offscreen_buffer *Buffer, int x, int y, char *Message, int Length)
{
    int CurrentPlayer_FontSize = 18;
    int CurrentPlayer_StartX = x;
    int CurrentPlayer_StartY = y;
    int CurrentPlayer_PaddingY = 0;
    int XOffset = WriteText(
        Game, 
        Buffer, 
        Message,
        Length, 
        CurrentPlayer_FontSize, 
        CurrentPlayer_StartX + 10,
        CurrentPlayer_StartY + CurrentPlayer_FontSize + CurrentPlayer_PaddingY,
        0xFFFFFFFF);
}

void
CountPlayerDiscs(int discs[64], int *player1, int *player2)
{
    for (int i = 0; i < 64; i++)
    {
        if (discs[i] == 1)
        {
            *player1 += 1;
        }
        else if (discs[i] == 2)
        {
            *player2 += 1;
        }
    }
}

int
HasValidMoves(int discs[64], int player)
{
    int *disc = discs;
    int cellCountPerSide = 8;
    int foundValidMoves = 0;
    for (int y = 0; y < cellCountPerSide; y++)
    {
        for (int x = 0; x < cellCountPerSide; x++)
        {
            int cell = cellCountPerSide * y + x;
            if (*disc++ == 0)
            {
                if (IsValidMove(discs, cell, player))
                {
                    foundValidMoves = 1;
                    break;
                }
            }
        }
        if (foundValidMoves)
        {
            break;
        }
    }
    return foundValidMoves;
}

void
PushHistoryEntry(game_memory *Game)
{
    ++Game->State.CurrentHistoryPosition;
    int *newHistoryEntry = Game->State.History + Game->State.CurrentHistoryPosition * 64;
    memcpy(newHistoryEntry, Game->State.Discs, sizeof(int) * 64);
    Game->State.PlayerHistory[Game->State.CurrentHistoryPosition] = Game->State.CurrentPlayer;
}

void
PopHistoryEntry(game_memory *Game)
{
    if (Game->State.CurrentHistoryPosition > -1)
    {
        memcpy(Game->State.Discs, Game->State.History + Game->State.CurrentHistoryPosition * 64, sizeof(int) * 64);
        Game->State.CurrentPlayer = Game->State.PlayerHistory[Game->State.CurrentHistoryPosition];
        --Game->State.CurrentHistoryPosition;
    }
}

void
StartNewGame(game_memory *Game)
{
    int discs[64] =
    {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 2, 1, 0, 0, 0,
        0, 0, 0, 1, 2, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
    };
    memcpy(Game->State.Discs, discs, sizeof(int) * 64);
    Game->State.CurrentPlayer = 1;
    Game->State.CurrentHistoryPosition = -1;
    Game->State.GameDone = 0;
    Game->State.Winner = 0;
    Game->State.Player1Score = 0;
    Game->State.Player2Score = 0;
    PushHistoryEntry(Game);
}

void
RenderGameControlsDesc(game_memory *Game, game_offscreen_buffer *Buffer, int x, int y)
{
    RenderGameMessage(Game, Buffer, x, y + 30, "Game controls:", 15);
    RenderGameMessage(Game, Buffer, x + 20, y + 60, "Click to move", 14);
    RenderGameMessage(Game, Buffer, x + 20, y + 80, "'U' to undo", 12);
    RenderGameMessage(Game, Buffer, x + 20, y + 100, "'N' to start a new game", 24);
}

int
RenderBoard(int *discs,
            int *hoverDiscs,
            game_offscreen_buffer *Buffer, 
            game_memory *Game,
            int cellCountPerSide, 
            int cellSize, 
            int cellColor, 
            int boardOriginX, 
            int boardOriginY, 
            int mouseX, 
            int mouseY, 
            bool render)
{
    int hoveredCell = -1;
    // Render board
    for (int y = 0; y < cellCountPerSide; y++)
    {
        for (int x = 0; x < cellCountPerSide; x++)
        {
            int cellX = boardOriginX + x * (cellSize - 1);
            if (cellX < 0) cellX = 0;
            int cellY = boardOriginY + y * (cellSize - 1);
            if (cellY < 0) cellY = 0;
            int thisCellColor = cellColor;
            #if 1
            int borderColor = 0xFFFFFF;
            #else
            int borderColor = 0x000000;
            if (y % 2) borderColor = 0x00DD00;
            else if (y % 3) borderColor = 0xDD0000;
            else if (x % 2) borderColor = 0x0000DD;
            else if (x % 3) borderColor = 0xFFFFFF;
            #endif
            int discValue = discs[y * cellCountPerSide + x];
            int hoverDiscValue = hoverDiscs[y * cellCountPerSide + x];
            if (discValue == 0 && mouseX > cellX && mouseX < cellX + cellSize &&
                mouseY > cellY && mouseY < cellY + cellSize)
            {
                hoveredCell = y * cellCountPerSide + x;
                #if 0
                thisCellColor = 0x006600;
                if (Game->State.UserInput.MouseLeftButtonClicked)
                {
                    thisCellColor = cellColor;
                }
                else if (Game->State.UserInput.MouseLeftButtonIsDown)
                {
                    thisCellColor = 0x008800;
                }
                #endif
            }

            if (render)
            {
                RenderRectangle(Buffer, borderColor, thisCellColor, cellX, cellY, cellSize, cellSize);
                if (discValue > 0)
                {
                    int centerX = (int)(cellX + (real32)cellSize / 2.0);
                    int centerY = (int)(cellY + (real32)cellSize / 2.0);

                    if (hoverDiscValue == 1)
                    {
                        RenderCircle(Buffer, 0x333333, 0x333333, centerX, centerY, 30, 80);
                    }
                    else if (hoverDiscValue == 2)
                    {
                        RenderCircle(Buffer, 0xAAAAAA, 0xAAAAAA, centerX, centerY, 30, 80);
                    }
                    else if (discValue == 1)
                    {
                        RenderCircle(Buffer, 0x000000, 0x000000, centerX, centerY, 30, 80);
                    }
                    else if (discValue == 2)
                    {
                        RenderCircle(Buffer, 0xFFFFFF, 0xFFFFFF, centerX, centerY, 30, 80);
                    }
                }
            }
        }
    }

    return hoveredCell;
}

void
GameUpdateAndRender(game_memory *Game, game_offscreen_buffer *Buffer)
{
    if (!Game->State.DiscsInitialized)
    {
        StartNewGame(Game);
        Game->State.DiscsInitialized = true;
    }
    else if (Game->State.UserInput.NButtonPressed)
    {
        StartNewGame(Game);
        Game->State.UserInput.NButtonPressed = false;
    }

    RenderRectangle(Buffer, 0x000000, 0x1B1B1B, 0, 0, Buffer->Width, Buffer->Height);

    int cellCountPerSide = 8;
    int cellSize = 80;
    int cellBorder = 1;
    int boardSize = cellSize * cellCountPerSide + cellBorder * (cellCountPerSide + 1);
    int cellColor = 0x004400;
    int cellBorderColor = 0xEEEEEE;
    int boardOriginX = 10;
    int boardOriginY = 10;
    
    int moveWasMade = 0;

    if (Game->State.UserInput.UButtonPressed)
    {
        PopHistoryEntry(Game);
        Game->State.UserInput.UButtonPressed = 0;
        Game->State.GameDone = 0;
    }

    for (int i = 0; i < cellCountPerSide * cellCountPerSide; i++)
    {
        Game->State.HoverDiscs[i] = 0;
    }
    int hoveredCell = RenderBoard(Game->State.Discs,
                                  Game->State.HoverDiscs,
                                  Buffer,
                                  Game,
                                  cellCountPerSide,
                                  cellSize,
                                  cellColor,
                                  boardOriginX,
                                  boardOriginY,
                                  Game->State.UserInput.MouseX,
                                  Game->State.UserInput.MouseY,
                                  false);

#if 0

    // Render board
    for (int y = 0; y < cellCountPerSide; y++)
    {
        for (int x = 0; x < cellCountPerSide; x++)
        {
            int cellX = boardOriginX + x * (cellSize - 1);
            if (cellX < 0) cellX = 0;
            int cellY = boardOriginY + y * (cellSize - 1);
            if (cellY < 0) cellY = 0;
            int thisCellColor = cellColor;
            #if 1
            int borderColor = 0xFFFFFF;
            #else
            int borderColor = 0x000000;
            if (y % 2) borderColor = 0x00DD00;
            else if (y % 3) borderColor = 0xDD0000;
            else if (x % 2) borderColor = 0x0000DD;
            else if (x % 3) borderColor = 0xFFFFFF;
            #endif
            int discValue = Game->State.Discs[y * cellCountPerSide + x];
            if (discValue == 0 && Game->State.UserInput.MouseX > cellX && Game->State.UserInput.MouseX < cellX + cellSize &&
                Game->State.UserInput.MouseY > cellY && Game->State.UserInput.MouseY < cellY + cellSize)
            {
                hoveredCell = y * cellCountPerSide + x;
                thisCellColor = 0x006600;
                if (Game->State.UserInput.MouseLeftButtonClicked)
                {
                    thisCellColor = cellColor;
                    if (IsValidMove(Game->State.Discs, hoveredCell, Game->State.CurrentPlayer))
                    {
                        PushHistoryEntry(Game);
                        FlipDiscs(Game->State.Discs, hoveredCell, Game->State.CurrentPlayer, MOVE);
                        Game->State.CurrentPlayer ^= 3;
                        moveWasMade = 1;
                    }

                }
                else if (Game->State.UserInput.MouseLeftButtonIsDown)
                {
                    thisCellColor = 0x008800;
                }
            }
            RenderRectangle(Buffer, borderColor, thisCellColor, cellX, cellY, cellSize, cellSize);
            if (discValue > 0)
            {
                int centerX = (int)(cellX + (real32)cellSize / 2.0);
                int centerY = (int)(cellY + (real32)cellSize / 2.0);

                if (discValue == 1)
                {
                    RenderCircle(Buffer, 0x000000, 0x000000, centerX, centerY, 30, 80);
                }
                else if (discValue == 2)
                {
                    RenderCircle(Buffer, 0xFFFFFF, 0xFFFFFF, centerX, centerY, 30, 80);
                }
                else if (discValue == -1)
                {
                    RenderCircle(Buffer, 0x333333, 0x333333, centerX, centerY, 30, 80);
                }
                else if (discValue == -2)
                {
                    RenderCircle(Buffer, 0xAAAAAA, 0xAAAAAA, centerX, centerY, 30, 80);
                }
            }
        }
    }
#endif

    if (hoveredCell > -1)
    {
        if (IsValidMove(Game->State.Discs, hoveredCell, Game->State.CurrentPlayer))
        {
            if (Game->State.UserInput.MouseLeftButtonClicked)
            {
                PushHistoryEntry(Game);
                FlipDiscs(Game->State.Discs, Game->State.HoverDiscs, hoveredCell, Game->State.CurrentPlayer, MOVE);
                Game->State.CurrentPlayer ^= 3;
                moveWasMade = 1;
            }
            else
            {
                FlipDiscs(Game->State.Discs, Game->State.HoverDiscs, hoveredCell, Game->State.CurrentPlayer, HOVER);
            }
        }
    }

    RenderBoard(Game->State.Discs,
                Game->State.HoverDiscs,
                  Buffer,
                  Game,
                  cellCountPerSide,
                  cellSize,
                  cellColor,
                  boardOriginX,
                  boardOriginY,
                  Game->State.UserInput.MouseX,
                  Game->State.UserInput.MouseY,
                  true);

    if (moveWasMade)
    {
        if (!HasValidMoves(Game->State.Discs, Game->State.CurrentPlayer))
        {
            if (!HasValidMoves(Game->State.Discs, Game->State.CurrentPlayer ^ 3))
            {
                int player1 = 0;
                int player2 = 0;
                CountPlayerDiscs(Game->State.Discs, &player1, &player2);
                if (player1 > player2)
                {
                    Game->State.Winner = 1;
                }
                else if (player2 > player1)
                {
                    Game->State.Winner = 2;
                }
                else
                {
                    Game->State.Winner = 0;
                }
                Game->State.Player1Score = player1;
                Game->State.Player2Score = player2;
                Game->State.GameDone = 1;
            }
            else
            {
                if (Game->State.CurrentPlayer == 1)
                {
                    char Message[32] = "No moves left for player Black!";
                    RenderGameMessage(Game, Buffer, boardSize, 50, Message, 32);
                    Game->State.CurrentPlayer = 2;
                    Game->State.LastPlayerBlocked = 1;
                }
                else
                {
                    char Message[32] = "No moves left for player White!";
                    RenderGameMessage(Game, Buffer, boardSize, 50, Message, 32);
                    Game->State.CurrentPlayer = 1;
                    Game->State.LastPlayerBlocked = 1;
                }
            }
        }
        else
        {
            Game->State.LastPlayerBlocked = 0;
        }
    }
    else if (!Game->State.GameDone)
    {
        if (Game->State.LastPlayerBlocked)
        {
            if (Game->State.CurrentPlayer == 1)
            {
                char Message[32] = "No moves left for player White!";
                RenderGameMessage(Game, Buffer, boardSize, 50, Message, 32);
            }
            else
            {
                char Message[32] = "No moves left for player Black!";
                RenderGameMessage(Game, Buffer, boardSize, 50, Message, 32);
            }
        }
    }

    RenderGameInfo(Game, Buffer, boardSize, 20);
    RenderGameControlsDesc(Game, Buffer, boardSize, 80);
}





