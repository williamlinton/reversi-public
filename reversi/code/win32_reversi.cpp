/* **************************************************************************
   $File: $
   $Date: $
   $Revision: $
   $Creator: William Linton $
   $Notice: (C) Copyright 2015 by William Linton, Inc. All Rights Reserved. $
   ************************************************************************** */

#include <windows.h>
#include <windowsx.h>
#include <stdio.h>
#include <dsound.h>
#include <math.h>
#include <gl\gl.h>

#define internal static
#define local_persist static
#define global_variable static

#define USE_OPENGL 1

#define Pi32 3.14159265359f

#define ArrayCount(Array) (sizeof(Array) / sizeof(Array[0]))

#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);

#include "win32_reversi.h"
#include "reversi.h"
#include "util.cpp"
#include "reversi_text.cpp"
#include "reversi.cpp"

global_variable LPDIRECTSOUNDBUFFER GlobalSecondaryBuffer;

struct win32_offscreen_buffer
{
    // Pixels are always 32 bits wide
    BITMAPINFO Info;
    void *Memory;
    int Width;
    int Height;
    int Pitch;
    int BytesPerPixel;
};

struct win32_window_dimension
{
    int Width;
    int Height;
};

global_variable bool GlobalRunning;
global_variable win32_offscreen_buffer GlobalBackBuffer;
global_variable int64 GlobalPerfCountFrequency;

inline LARGE_INTEGER
Win32GetWallClock(void)
{
    LARGE_INTEGER Result;
    QueryPerformanceCounter(&Result);
    return(Result);
}

inline real32
Win32GetSecondsElapsed(LARGE_INTEGER Start, LARGE_INTEGER End)
{
    real32 Result = ((real32)(End.QuadPart - Start.QuadPart) /
            (real32)GlobalPerfCountFrequency);
    return(Result);
}

internal void
Win32DisplayBufferInWindow(HDC DeviceContext,
        int WindowWidth, int WindowHeight,
        win32_offscreen_buffer *Buffer)
{
#ifndef USE_OPENGL
    StretchDIBits(DeviceContext,
            /*
             X, Y, Width, Height,
             X, Y, Width, Height,
             */
            0, 0, WindowWidth, WindowHeight,
            0, 0, Buffer->Width, Buffer->Height,
            Buffer->Memory,
            &Buffer->Info,
            DIB_RGB_COLORS, SRCCOPY);
#else
    glViewport(0, 0, WindowWidth, WindowHeight);

    GLuint TextureHandle;
    static int Init = 0;
    if (!Init)
    {
        glGenTextures(1, &TextureHandle);
        Init = 1;
    }

    glBindTexture(GL_TEXTURE_2D, TextureHandle);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, Buffer->Width, Buffer->Height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, Buffer->Memory);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
/*    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL); */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_PRIORITY);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE);

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glEnable(GL_TEXTURE_2D);

    glClearColor(1.0f, 0.0f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glMatrixMode(GL_PROJECTION);
    //glLoadIdentity();

    real32 a = 2.0f / (real32)Buffer->Width;
    real32 b = 2.0f / (real32)Buffer->Height;
    real32 Proj[] = 
    {
         a,  0,  0,  0,
         0, -b,  0,  0,
         0,  0,  1,  0,
        -1, 1,  0,  1
    };
    glLoadMatrixf(Proj);

    glBegin(GL_TRIANGLES);

    real32 MinX = 0;
    real32 MaxX = (real32)Buffer->Width;
    real32 MinY = 0;
    real32 MaxY = (real32)Buffer->Height;
    real32 P = 1.0;
    //glColor3f(1.0f, 0.0, 0.0);
    glTexCoord2f(0.0f, 0.0f);
    glVertex2f(MinX, MinY);
    //glColor3f(0.0f, 1.0, 0.0);
    glTexCoord2f(1.0f, 0.0f);
    glVertex2f(MaxX, MinY);
    //glColor3f(0.0f, 0.0, 1.0);
    glTexCoord2f(1.0f, 1.0f);
    glVertex2f(MaxX, MaxY);

    //glColor3f(1.0f, 1.0, 1.0);
    glTexCoord2f(0.0f, 0.0f);
    glVertex2f(MinX, MinY);
    glTexCoord2f(1.0f, 1.0f);
    glVertex2f(MaxX, MaxY);
    glTexCoord2f(0.0f, 1.0f);
    glVertex2f(MinX, MaxY);
    glEnd();

    SwapBuffers(DeviceContext);
#endif
}

win32_window_dimension
Win32GetWindowDimension(HWND Window)
{
    win32_window_dimension Result;

    RECT ClientRect;
    GetClientRect(Window, &ClientRect);
    Result.Width = ClientRect.right - ClientRect.left;
    Result.Height = ClientRect.bottom - ClientRect.top;

    return(Result);
}

internal void
Win32ResizeDIBSection(win32_offscreen_buffer *Buffer, int Width, int Height)
{
    if(Buffer->Memory)
    {
        VirtualFree(Buffer->Memory, 0, MEM_RELEASE);
    }

    Buffer->Width = Width;
    Buffer->Height = Height;

    int BytesPerPixel = 4;

    Buffer->Info.bmiHeader.biSize = sizeof(Buffer->Info.bmiHeader);
    Buffer->Info.bmiHeader.biWidth = Buffer->Width;
    Buffer->Info.bmiHeader.biHeight = -Buffer->Height;
    Buffer->Info.bmiHeader.biPlanes = 1;
    Buffer->Info.bmiHeader.biBitCount = 32;
    Buffer->Info.bmiHeader.biCompression = BI_RGB;

    int BitmapMemorySize = (Buffer->Width*Buffer->Height)*BytesPerPixel;
    Buffer->Memory = VirtualAlloc(0, BitmapMemorySize, MEM_COMMIT, PAGE_READWRITE);
    Buffer->Pitch = Width*BytesPerPixel;
    Buffer->BytesPerPixel = BytesPerPixel;
}

internal void
Win32InitOpenGL(HWND Window)
{
    HDC WinDC = GetDC(Window);

    PIXELFORMATDESCRIPTOR DesiredPixelFormat = {};
    DesiredPixelFormat.nSize = sizeof(DesiredPixelFormat);
    DesiredPixelFormat.nVersion = 1;
    DesiredPixelFormat.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
    DesiredPixelFormat.cColorBits = 24;
    DesiredPixelFormat.cAlphaBits = 8;
    DesiredPixelFormat.iLayerType = PFD_MAIN_PLANE;

    int SuggestedPixelFormatIndex = ChoosePixelFormat(WinDC, &DesiredPixelFormat);
    PIXELFORMATDESCRIPTOR SuggestedPixelFormat;
    DescribePixelFormat(WinDC, SuggestedPixelFormatIndex, sizeof(SuggestedPixelFormat), &SuggestedPixelFormat);
    SetPixelFormat(WinDC, SuggestedPixelFormatIndex, &SuggestedPixelFormat);

    HGLRC OpenGLRC = wglCreateContext(WinDC);
    if (wglMakeCurrent(WinDC, OpenGLRC))
    {
    }
    else
    {
        // shouldn't happen
        // TODO: Diagnostic
    }
    ReleaseDC(Window, WinDC);
}

LRESULT CALLBACK
MainWindowCallback(HWND Window,
                   UINT Message,
                   WPARAM WParam,
                   LPARAM LParam)
{
   LRESULT Result = 0;

   switch(Message)
   {
      case WM_SIZE:
      {
         //OutputDebugString("WM_SIZE\n");
      } break;

      case WM_DESTROY:
      {
         //OutputDebugString("WM_DESTROY\n");
      } break;

      case WM_CLOSE:
      {
         //OutputDebugString("WM_CLOSE\n");
         GlobalRunning = false;
      } break;

      case WM_ACTIVATEAPP:
      {
         //OutputDebugString("WM_ACTIVATEAPP\n");
      } break;

      case WM_PAINT:
      {
         PAINTSTRUCT Paint;
         HDC DeviceContext = BeginPaint(Window, &Paint);
         win32_window_dimension Dimension = Win32GetWindowDimension(Window);
         Win32DisplayBufferInWindow(DeviceContext, Dimension.Width, Dimension.Height,
                 &GlobalBackBuffer);
         EndPaint(Window, &Paint);
      } break;

      case WM_LBUTTONDOWN:
      {
          //OutputDebugString("Left Mouse Button down!");
      } break;

      case WM_LBUTTONUP:
      {
          //OutputDebugString("Left Mouse Button up!");
      } break;

      case WM_MOUSEWHEEL:
      {

      } break;

      default:
      {
         //OutputDebugString("[unhandled]\n");
         Result = DefWindowProc(Window, Message, WParam, LParam);
      } break;
   }

   return(Result);
}

internal void
Win32CorrectMousePosition(HWND Window, game_state *GameState)
{
    RECT ClientRect;
    if (GetClientRect(Window, &ClientRect))
    {
        real32 XRatio = (real32)ClientRect.right / (real32)GlobalBackBuffer.Width;
        real32 YRatio = (real32)ClientRect.bottom / (real32)GlobalBackBuffer.Height;
        GameState->UserInput.MouseX = (int32)((real32)GameState->UserInput.MouseX / XRatio);
        GameState->UserInput.MouseY = (int32)((real32)GameState->UserInput.MouseY / YRatio);
        GameState->UserInput.StartedDraggingAtMouseX = (int32)((real32)GameState->UserInput.StartedDraggingAtMouseX / XRatio);
        GameState->UserInput.StartedDraggingAtMouseY = (int32)((real32)GameState->UserInput.StartedDraggingAtMouseY / YRatio);
    }
}

internal void
Win32ProcessPendingMessages(HWND Window, game_state *GameState)
{
    MSG Message;
    while (PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
    {
        switch (Message.message)
        {
            case WM_QUIT:
            {
                GlobalRunning = false;
            } break;

            case WM_LBUTTONDOWN:
            {
                GameState->UserInput.MouseLeftButtonIsDown = true;
                GameState->UserInput.MouseX = GET_X_LPARAM(Message.lParam); 
                GameState->UserInput.MouseY = GET_Y_LPARAM(Message.lParam); 
                GameState->UserInput.StartedDraggingAtMouseX = GET_X_LPARAM(Message.lParam); 
                GameState->UserInput.StartedDraggingAtMouseY = GET_Y_LPARAM(Message.lParam); 
                Win32CorrectMousePosition(Window, GameState);
                //OutputDebugString("Left Mouse Button down!");
            } break;

            case WM_LBUTTONUP:
            {
                GameState->UserInput.MouseLeftButtonIsDown = false;
                GameState->UserInput.MouseLeftButtonClicked = true;
                GameState->UserInput.MouseX = GET_X_LPARAM(Message.lParam); 
                GameState->UserInput.MouseY = GET_Y_LPARAM(Message.lParam); 
                Win32CorrectMousePosition(Window, GameState);
                //OutputDebugString("Left Mouse Button up!");
            } break;

            case WM_MOUSEMOVE:
            {
                GameState->UserInput.MouseX = GET_X_LPARAM(Message.lParam); 
                GameState->UserInput.MouseY = GET_Y_LPARAM(Message.lParam); 
                Win32CorrectMousePosition(Window, GameState);
            } break;

            case WM_MOUSEWHEEL:
            {
                // If no other keys are pressed, allow mousewheel input.
                if (GET_KEYSTATE_WPARAM(Message.wParam) == 0)
                {
                    GameState->UserInput.MouseWheelDelta += GET_WHEEL_DELTA_WPARAM(Message.wParam);
                    GameState->UserInput.MouseX = (int)(short)GET_X_LPARAM(Message.lParam); 
                    GameState->UserInput.MouseY = (int)(short)GET_Y_LPARAM(Message.lParam); 
                    Win32CorrectMousePosition(Window, GameState);
                }
            } break;

            default:
            {
                TranslateMessage(&Message);
                DispatchMessageA(&Message);
            } break;
        }
     }          
}

int CALLBACK
WinMain(HINSTANCE Instance,
		HINSTANCE PrevInstance,
		LPSTR CommandLine,
		int ShowCode)
{
    WNDCLASS WindowClass = {};

    LARGE_INTEGER PerfCountFrequencyResult;
    QueryPerformanceFrequency(&PerfCountFrequencyResult);
    GlobalPerfCountFrequency = PerfCountFrequencyResult.QuadPart;

    UINT DesiredSchedulerMS = 1;
    bool32 SleepIsGranular = (timeBeginPeriod(DesiredSchedulerMS) == TIMERR_NOERROR);

    Win32ResizeDIBSection(&GlobalBackBuffer, 1280, 720);

    // TODO - Check if hredraw/vredraw/owndc still matter
    WindowClass.style = CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
    WindowClass.lpfnWndProc = MainWindowCallback;
    WindowClass.hInstance = Instance;
    //WindowClass.hIcon
    WindowClass.lpszClassName = "ReversiWindowClass";
    //MessageBox(0, "Welcome to ChordBuilder...", "ChordBuilder", MB_OK|MB_ICONASTERISK);

    if (RegisterClass(&WindowClass))
    {
        HWND Window = NULL;
        Window = CreateWindowExA(
                0,
                WindowClass.lpszClassName,
                "ChordBuilder",
                WS_OVERLAPPEDWINDOW|WS_VISIBLE,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                0,
                0,
                Instance,
                0);

        if (Window)
        {
            Win32InitOpenGL(Window);

            //------ Graphics ---------
            int MonitorRefreshHz = 60;

            HDC DeviceContext = GetDC(Window);
            int Win32RefreshRate = GetDeviceCaps(DeviceContext, VREFRESH);
            
            if(Win32RefreshRate > 1)
            {
                MonitorRefreshHz = Win32RefreshRate;
            }
            real32 GameUpdateHz = (MonitorRefreshHz / 1.0f);
            real32 TargetSecondsPerFrame = 1.0f / (real32)GameUpdateHz;

            LARGE_INTEGER FlipWallClock = Win32GetWallClock();

            game_memory Game = {0};


            //-------- Font Init ----------
            Game.FontData = (unsigned char *)VirtualAlloc(0, 1000000, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
            InitText(&Game);

            Game.State = {0};

            Game.State.Settings = {0};
            Game.State.Settings.VolumeNoteHovered = 500;
            Game.State.Settings.VolumeNoteOn = 2000;

            Game.State.LastNoteDelta = 0;
            Game.State.Notes = (int32 *)VirtualAlloc(0, NOTE_COUNT*4, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
            Game.State.NotesVolume = (real32 *)VirtualAlloc(0, NOTE_COUNT*sizeof(real32), MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
            Game.State.tSine = (real32 *)VirtualAlloc(0, NOTE_COUNT*sizeof(real32), MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
            Game.State.NotesInitialized = false;

            GlobalRunning = true;

            LARGE_INTEGER LastCounter = Win32GetWallClock();

            Game.State.UserInput = {};

            HCURSOR Cursor = LoadCursor(NULL, IDC_ARROW);
            SetCursor(Cursor);
//            ShowCursor(TRUE);

            game_offscreen_buffer Buffer = {};
            Buffer.Memory = GlobalBackBuffer.Memory;
            Buffer.Width = GlobalBackBuffer.Width;
            Buffer.Height = GlobalBackBuffer.Height;
            Buffer.Pitch = GlobalBackBuffer.Pitch;
            Buffer.BytesPerPixel = GlobalBackBuffer.BytesPerPixel;

            while(GlobalRunning)
            {
                Win32ProcessPendingMessages(Window, &Game.State);

//                RenderStuff(GlobalBackBuffer, XOffset, YOffset);

                if (Game.State.UserInput.MouseLeftButtonClicked)
                {
                    //OutputDebugString("");
                }


                DWORD error = GetLastError();

#if 0
                POINT p;
                if (GetCursorPos(&p))
                {
HCURSOR WINAPI LoadCursor(
  _In_opt_ HINSTANCE hInstance,
  _In_     LPCTSTR   lpCursorName
);
                    //cursor position now in p.x and p.y
                    if (ScreenToClient(Window, &p))
                    {
                        //p.x and p.y are now relative to hwnd's client area
                        Game.State.UserInput.MouseX = p.x;
                        Game.State.UserInput.MouseY = p.y;
                    }
                } 
#endif


                LARGE_INTEGER MWDCounter = Win32GetWallClock();
                Game.State.UserInput.MouseWheelDeltaClock = Win32GetSecondsElapsed(FlipWallClock, MWDCounter);

                GameUpdateAndRender(&Game, TargetSecondsPerFrame, &Buffer);

                // Now that we handled the click, we can clear it
                Game.State.UserInput.MouseLeftButtonClicked = false;
                //Game.State.UserInput.MouseWheelDelta = 0;

                LARGE_INTEGER WorkCounter = Win32GetWallClock();
                real32 WorkSecondsElapsed = Win32GetSecondsElapsed(LastCounter, WorkCounter);

                real32 SecondsElapsedForFrame = WorkSecondsElapsed;
                if(SecondsElapsedForFrame < TargetSecondsPerFrame)
                {
                    if(SleepIsGranular)
                    {
                        DWORD SleepMS = (DWORD)(1000.0f * (TargetSecondsPerFrame -
                                    SecondsElapsedForFrame));

                        if(SleepMS > 0)
                        {
                            Sleep(SleepMS);
                        }
                    }

                    real32 TestSecondsElapsedForFrame = Win32GetSecondsElapsed(LastCounter,
                            Win32GetWallClock());

                    if(TestSecondsElapsedForFrame < TargetSecondsPerFrame)
                    {

                    }

                    while(SecondsElapsedForFrame < TargetSecondsPerFrame)
                    {
                        SecondsElapsedForFrame = Win32GetSecondsElapsed(LastCounter,
                                Win32GetWallClock());
                    }
                }
                else
                {

                }

                LARGE_INTEGER EndCounter = Win32GetWallClock();
                real32 MSPerFrame = 1000.0f*Win32GetSecondsElapsed(LastCounter, EndCounter);
                LastCounter = EndCounter;

                real64 FPS = 1000.0/MSPerFrame;

                char FPSBuffer[256];
                _snprintf_s(FPSBuffer, sizeof(FPSBuffer),
                        "%.02fms/f, %.02ff/s\n", MSPerFrame, FPS);
                //OutputDebugStringA(FPSBuffer);

                win32_window_dimension Dimension = Win32GetWindowDimension(Window);
                DeviceContext = GetDC(Window);
                Win32DisplayBufferInWindow(DeviceContext, Dimension.Width, Dimension.Height,
                    &GlobalBackBuffer);
                ReleaseDC(Window, DeviceContext);
            }
        }
    }
    else
    {}

    return(0);
}

