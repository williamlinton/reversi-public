/* **************************************************************************
   $File: $
   $Date: $
   $Revision: $
   $Creator: William Linton $
   $Notice: (C) Copyright 2015 by William Linton, Inc. All Rights Reserved. $
   ************************************************************************** */

#define NOTE_COUNT 128

#if CHORDBUILDER_SLOW
// TODO(casey): Complete assertion macro - don't worry everyone!
#define Assert(Expression) if(!(Expression)) {*(int *)0 = 0;}
#else
#define Assert(Expression)
#endif

struct user_input
{
    bool32 MouseLeftButtonIsDown;
    bool32 MouseLeftButtonClicked;
    int32 MouseX;
    int32 MouseY;
    int32 StartedDraggingAtMouseX;
    int32 StartedDraggingAtMouseY;
    int32 MouseWheelDelta;
    real32 MouseWheelDeltaClock;
    real32 dtMouseWheelIn;
    real32 dtMouseWheelOut;
    bool32 UButtonPressed;
    bool32 NButtonPressed;
};

struct game_offscreen_buffer
{
    void *Memory;
    int Width;
    int Height;
    int Pitch;
    int BytesPerPixel;
};

struct game_settings
{
    real32 VolumeNoteHovered;
    real32 VolumeNoteOn;
    int DebugWaveFormsEnabled;
};

struct board
{
    int SizePerSide;
    int *Discs;
};

struct game_state
{
    game_settings Settings;
    real32 LastNoteDelta;
    real32 NotePosX;
    bool32 NotesInitialized;
    user_input UserInput;
    real32 tSine1;
    real32 tSine2;
    int *Notes;
    int *NotesVolumeState;
    real32 *NotesVolume;
    real32 *tSine;
    int ViewportOffsetY;
    int ViewportLastDragYPos;
    int ViewportIsBeingDragged;
    bool32 DiscsInitialized;
    int Discs[64];
    int HoverDiscs[64];
    int CurrentPlayer;
    int Winner;
    int GameDone;
    int Player1Score;
    int Player2Score;
    int LastPlayerBlocked;
    int CurrentHistoryPosition;
    int *History;
    int PlayerHistory[64];
};

struct game_memory
{
    game_state State;
    unsigned char *FontData;
    unsigned char *BitmapWorkspace;
    game_offscreen_buffer *TempTextRenderBuffer;
};

struct game_sound_output_buffer
{
    int SamplesPerSecond;
    int SampleCount;
    int16 *Samples;
};

enum note_state
{
    OFF,
    HOVERED,
    ON
};

enum volume_state
{
    VOLUME_OFF,
    VOLUME_FADING_IN,
    VOLUME_FULL,
    VOLUME_FADING_OUT
};
