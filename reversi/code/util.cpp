

int
Append(char *addendum, char *existingString, int limit = 1024)
{
    int found = 0;
    int i;
    for (i = 0; i < limit; i++)
    {
        if (existingString[i] == '\0')
        {
            found = 1;
            break;
        }
    }

    if (found)
    {
        strcpy(&existingString[i], addendum);
    }
    else
    {
        return 0;
    }

    return 1;
}
