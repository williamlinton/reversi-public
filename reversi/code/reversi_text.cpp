#if 0
#include <ft2build.h>
#include FT_FREETYPE_H

FT_Library Library;
#endif


#define STB_TRUETYPE_IMPLEMENTATION  // force following include to generate implementation
#include "stb_truetype.h"

internal
stbtt_fontinfo FontInfo;

int
GetKerning(char FirstChar, char SecondChar, int FontSize)
{
#if 1
    if (FontInfo.kern)
    {
        int iFirstChar = (int)FirstChar;
        int iSecondChar = (int)SecondChar;
        int kerning = stbtt_GetCodepointKernAdvance(&FontInfo, iFirstChar, iSecondChar);
        return kerning;
    }
#endif

    return (int)(0.1 * (float)FontSize);
}

uint8
InterpolateUint8(uint8 First, uint8 Second, float Percentage)
{
    if (Second > First)
    {
        return First + (uint8)((float)(Second - First) * Percentage);
    }

    return First - (uint8)((float)(First - Second) * Percentage);
}

void
InitText(game_memory *Game)
{
    int i, j, ascent, baseline, ch = 0;
    float xpos = 2;
    //int bytes_read = fread(Game->FontData, 1, 218000, fopen("OpenSans-Regular.ttf", "rb"));
    //int bytes_read = fread(Game->FontData, 1, 170000, fopen("Roboto-Medium.ttf", "rb"));
    //int bytes_read = fread(Game->FontData, 1, 1600000, fopen("FreeSans.ttf", "rb"));
    int bytes_read = fread(Game->FontData, 1, 170000, fopen("Roboto-Light.ttf", "rb"));
    stbtt_InitFont(&FontInfo, Game->FontData, stbtt_GetFontOffsetForIndex(Game->FontData,0));
//    baseline = (int) (ascent*scale);
}

void
AlphaBlend(uint32 *source, uint32 *dest, int x, int y, int width, int height, int pitch, real32 opacity)
{
    int offset = pitch * y + x;
    uint32 *SourcePixel = source + offset;
    uint32 *DestPixel = dest + offset;

    int Row = 0;
    for (Row = 0; Row < height; ++Row)
    {
        int Column;
        for (Column = 0; Column < width; ++Column)
        {
            uint32 ExistingColor = *DestPixel;
            uint8 ExistingBlue = ExistingColor & 0x000000FF;
            uint8 ExistingGreen = ExistingColor >> 8 & 0x000000FF;
            uint8 ExistingRed = ExistingColor >> 16 & 0x000000FF;

            uint32 SourceColor = *SourcePixel;
            uint8 SourceBlue =  SourceColor & 0x000000FF;
            uint8 SourceGreen = SourceColor >> 8 & 0x000000FF;
            uint8 SourceRed =   SourceColor >> 16 & 0x000000FF;

            uint8 Blue = InterpolateUint8(ExistingBlue, SourceBlue, opacity);
            uint8 Green = InterpolateUint8(ExistingGreen, SourceGreen, opacity);
            uint8 Red = InterpolateUint8(ExistingRed, SourceRed, opacity);
            #if 0
            unsigned char Value = Bitmap[Row * Width + Column];
            uint8 Blue = Value;
            uint8 Green = Value;
            uint8 Red = Value;
            #endif

            #if 0
            Red = SourceRed;
            Green = SourceGreen;
            Blue = SourceBlue;
            Red = ExistingRed;
            Green = ExistingGreen;
            Blue = ExistingBlue;
            #endif

            *DestPixel = (((Red << 16) | (Green << 8)) | Blue);

            ++SourcePixel;
            ++DestPixel;
        }

        int newOffset = (pitch * Row) + x;
        DestPixel += newOffset;
        SourcePixel += newOffset;
    }
}

struct glyph
{
    int width;
    int height;
    unsigned char *mem;
};

int
WriteText(game_memory *Game, game_offscreen_buffer *Buffer, char *String, int StringLength, int FontSize, int  XOffset, int YOffset, uint32 Color)
{
    int Index = 0;
    int ascent, descent, lineGap, baseline;
    real32 scale = stbtt_ScaleForPixelHeight(&FontInfo, FontSize);
    stbtt_GetFontVMetrics(&FontInfo, &ascent, &descent, &lineGap);
    baseline = (int) (ascent * scale);

    game_offscreen_buffer *TempBuffer = Game->TempTextRenderBuffer;
    TempBuffer = Buffer;

    int originalXOffset = XOffset;
    int originalYOffset = YOffset;

    int usedHeight = 0;
    int usedWidth = 0;

    uint8 DesiredBlue = Color & 0x000000FF;
    uint8 DesiredGreen = Color >> 8 & 0x000000FF;
    uint8 DesiredRed = Color >> 16 & 0x000000FF;

    
    while (String != '\0' && Index++ < StringLength)
    {
        int Width, Height;
        int LetterXOffset, LetterYOffset;
        int Advance, Lsb;

        if (*String == ' ')
        {
            XOffset += (int)((float)FontSize * 0.5);
            ++String;
            continue;
        }

        int Letter = (int)*String;
        unsigned char *Bitmap = stbtt_GetCodepointBitmap(
            &FontInfo, 
            0, 
            scale, Letter, &Width, &Height, &LetterXOffset, &LetterYOffset);

        int LetterAscent = -LetterYOffset + 1;
        int StartY = baseline - LetterAscent;
        int CurrentYOffset = YOffset + StartY;

        unsigned char *SourcePixel = Bitmap;
        //uint32 *DestPixel = (uint32 *)Buffer->Memory;
        int Row;

       // *Pixel = Color;

        int currentWidth = 0;
        int currentHeight = 0;
        uint8 *BufferRow = (uint8 *)TempBuffer->Memory + (CurrentYOffset*TempBuffer->Width)*TempBuffer->BytesPerPixel;
        for (Row = 0; Row < Height; ++Row)
        {
            ++currentHeight;
            uint32 *DestPixel = (uint32 *)BufferRow + XOffset;
            int Column;
            for (Column = 0; Column < Width; ++Column)
            {
                uint32 ExistingColor = *DestPixel;
                uint8 ExistingBlue = ExistingColor & 0x000000FF;
                uint8 ExistingGreen = ExistingColor >> 8 & 0x000000FF;
                uint8 ExistingRed = ExistingColor >> 16 & 0x000000FF;

                uint8 GrayScaleValue = *SourcePixel;
                float Percentage = (float)GrayScaleValue / 255;

                uint8 Blue = InterpolateUint8(ExistingBlue, DesiredBlue, Percentage);
                uint8 Green = InterpolateUint8(ExistingGreen, DesiredGreen, Percentage);
                uint8 Red = InterpolateUint8(ExistingRed, DesiredRed, Percentage);
                #if 0
                unsigned char Value = Bitmap[Row * Width + Column];
                uint8 Blue = Value;
                uint8 Green = Value;
                uint8 Red = Value;
                #endif

                *DestPixel = (((Red << 16) | (Green << 8)) | Blue);

                ++SourcePixel;
                ++DestPixel;
            }
            currentWidth = Column + 1;

            BufferRow += TempBuffer->Pitch;
        }

        usedWidth += Width;

        if (Height > usedHeight)
        {
            usedHeight = Height;
        }

        stbtt_FreeBitmap(Bitmap, 0);

        char FirstChar = *String;

        ++String;

        char SecondChar = *String;
        XOffset += Width;

        if (SecondChar)
        {
            XOffset += GetKerning(FirstChar, SecondChar, FontSize);
        }
    }

    //AlphaBlend((uint32 *)TempBuffer->Memory, (uint32 *)Buffer->Memory, originalXOffset, originalYOffset, usedWidth, usedHeight, Buffer->Pitch / Buffer->BytesPerPixel, 0.1);

    return XOffset;
}
// allocates a large-enough single-channel 8bpp bitmap and renders the
// specified character/glyph at the specified scale into it, with
// antialiasing. 0 is no coverage (transparent), 255 is fully covered (opaque).
// *width & *height are filled out with the width & height of the bitmap,
// which is stored left-to-right, top-to-bottom.
//
// xoff/yoff are the offset it pixel space from the glyph origin to the top-left of the bitmap
    

unsigned char buffer[24<<20];
unsigned char screen[20][79];
void
TestFonts()
{
   stbtt_fontinfo font;
   int i,j,ascent,baseline,ch=0;
   float scale, xpos=2; // leave a little padding in case the character extends left
   char *text = "Heljo World!"; // intentionally misspelled to show 'lj' brokenness
   fread(buffer, 1, 1000000, fopen("c:/windows/fonts/arialbd.ttf", "rb"));
   stbtt_InitFont(&font, buffer, 0);
   scale = stbtt_ScaleForPixelHeight(&font, 15);
   stbtt_GetFontVMetrics(&font, &ascent,0,0);
   baseline = (int) (ascent*scale);
   while (text[ch]) {
      int advance,lsb,x0,y0,x1,y1;
      float x_shift = xpos - (float) floor(xpos);
      stbtt_GetCodepointHMetrics(&font, text[ch], &advance, &lsb);
      stbtt_GetCodepointBitmapBoxSubpixel(&font, text[ch], scale,scale,x_shift,0, &x0,&y0,&x1,&y1);
      stbtt_MakeCodepointBitmapSubpixel(&font, &screen[baseline + y0][(int) xpos + x0], x1-x0,y1-y0, 79, scale,scale,x_shift,0, text[ch]);
      // note that this stomps the old data, so where character boxes overlap (e.g. 'lj') it's wrong
      // because this API is really for baking character bitmaps into textures. if you want to render
      // a sequence of characters, you really need to render each bitmap to a temp buffer, then
      // "alpha blend" that into the working buffer
      xpos += (advance * scale);
      if (text[ch+1])
         xpos += scale*stbtt_GetCodepointKernAdvance(&font, text[ch],text[ch+1]);
      ++ch;
   }
   for (j=0; j < 20; ++j) {
      for (i=0; i < 78; ++i)
         putchar(" .:ioVM@"[screen[j][i]>>5]);
      putchar('\n');
   }
}
