# Reversi

If you just want to play the game, skip to Emscripten -> How to run.

This is a Reversi clone I wrote in C++ (mostly C, possibly with some C++ features, but I don't remember which ones). The motivation was partly to gain more experience with C/C++, OpenGL, WebGL, SDL, and Emscripten, and partly because I love the Reversi game.

It's been a while since I wrote this, so I'm not sure what the current state of the code is. It's possible some things don't work due to changes or experiments I was making. The present builds currently work as described below, but if you were to recompile them, I don't know whether they would break.

Note: There's no AI. The game assumes both players are human.

Also note that I originally used Mercurial for source control. Since I wasn't sure about the status of the code, I wanted to include the current binaries so I could publish a working version quickly. I used git to package it up from a higher root and not worry about nested repositories.

I loosely followed a lot of what I learned from Casey Muratori's Handmade Hero series -- most notably the build system and general game architecture (and possibly some of the display code).

From what I can tell, I had 3 compilation targets:

- Emscripten
- Win32
- SDL

## Emscripten
### Description
The Emscripten version is the most stable and full-featured. It's possible to play a full game, including final scores. Everything works except for the keyboard command to start a new game. (The undo command is an upper-case U.)

### How to run
Run .\run.bat, then hit http://localhost:8000/reversi2.html in a browser.

Note: If you don't have python installed, use any web server to avoid CORS errors.

## Win32
### Description
The Win32 version was the earliest, I believe. 

### How to run
Run .\build\win32_reversi.exe

## SDL
With the SDL version, I believe I was trying out an experimental feature where the game would show you all your possible moves. It seems to be broken and crashes easily.

### How to run
Run .\build\sdl_reversi.exe
